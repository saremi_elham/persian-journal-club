# Persian Journal Club

## May 10th, 2022
   - **Zahra S.**: Getting started with Maneage, session 6 \
     (_Execution of tutorial commands by Zohreh._) \
     (_How to complete the latex part of a make-file at maneage by Sepideh and Pedram._)

--------------------------------------------------------------

## May 3rd, 2022
   - **Mohammad**: Getting started with Maneage, session 5 \
     (_Setting of input data in project._) 

--------------------------------------------------------------

## April 19th, 2022
   - **Mohammad**: Getting started with Maneage, session 4 \
     (_An overview of the README-hacking file._) \
     (_Merging with Maneage and importing updates._)

--------------------------------------------------------------

## April 12th, 2022
   - **Mohammad**: Getting started with Maneage, session 3 \
     (_An overview of the README-hacking file._) \
     (_Elham's project was used as a base-line. The correct way to enter references in the paper was examined._)

--------------------------------------------------------------

## March 29th, 2022
   - **Mohammad**: Getting started with Maneage, session 2 \
     (_An overview of the README-hacking file._)

--------------------------------------------------------------

## March 22nd, 2022
  - **CANCELLED** due to the Nowruz holidays.

--------------------------------------------------------------

## March 16th, 2022
   - **Mohammad**: Getting started with Maneage \
     http://maneage.org \
     https://www.youtube.com/watch?v=XdhRUhoMqw0 \
     https://www.youtube.com/watch?v=AtinDGUnhVw \
     (_Zohre's project will be used as a base-line, but everyone should try to follow in their projects._)

## March 8th, 2022

   - **Zohreh**: Arithmetic's Dimensionality changing operators \
     https://www.gnu.org/software/gnuastro/manual/html_node/Dimensionality-changing-operators.html

   - **Sepideh**: Make tip (Seconday expansion) \
      https://www.gnu.org/software/make/manual/html_node/Secondary-Expansion.html

   - **Mohammad**: Calling a Makefile within another Makefile

--------------------------------------------------------------

## March 1st, 2022

   - **Zohreh**: The Evolution of AGN Activity in Brightest Cluster Galaxies \
                 https://arxiv.org/abs/2201.08398?context=astro-ph

   - **Elham**: Introducing AGB stars and their role in producing galaxy dust \
                https://doi.org/10.48550/arXiv.2112.14158 \
                https://link.springer.com/article/10.1007/s00159-017-0106-5

--------------------------------------------------------------

## February 22nd, 2022

   - **Elham**: Proposal submission for some telescopes that your institute isn't a member of\
                https://orp-h2020.eu/optical-call-2022b

   - **Sepideh**: Extended review of SMACK 1 in Persian \
                  https://akhlaghi.org/pdf/posix-family.pdf

   - **Zahra S.**: Section2: Resolved Stellar Populations as Tracers of Outskirts   \
                   https://link.springer.com/book/10.1007/978-3-319-56570-5 
                   
   - **Mohammad**: History of Galaxy Evolution \
                   https://www.astr.tohoku.ac.jp/~akhlaghi/blog/histgal.html

--------------------------------------------------------------

## February 15th, 2022:

   - **Elham**: Organizing the minutes of the meetings 

   - **Zahra S.**: Review the studies about the outer most part of the galaxies \
                   https://link.springer.com/book/10.1007/978-3-319-56570-5 

   - **Sepideh**: Ram pressure stripping in intermediate redshift clusters 
                  (Abell 2744 and Abell 370) \
                  https://arxiv.org/abs/2111.04501

   - **Mohammad**: A short hint to this paper: _Writing Scientific Papers in Astronomy_ \
                   https://arxiv.org/pdf/2110.05503.pdf

   - **Teymoor**: Introducing myself! 

 --------------------------------------------------------------

## February 8th, 2022:

   - **Mohammad**:  Discussion on new format 

   - **Zohreh**: A new Nature Astronomy review by Federico Lelli  \
                 https://www.nature.com/articles/s41550-021-01562-2.pdf
      - Related link: [SPARC](http://astroweb.cwru.edu/SPARC/)

 
   - **Mohammad**: Series of papers on JWST's NIRSpec instrument that came out today  \
                   https://arxiv.org/abs/2202.03305 (_Overview_) \
                   https://arxiv.org/abs/2202.03306 (_Multi-object spectroscopy_) \
                   https://arxiv.org/abs/2202.03308 (_Integral field spectroscopy_)
      - Related links: 
        1. [SPHEREx.Survey](https://spherex.caltech.edu/Survey.html)
        2. [SPHEREx.Instrument](https://spherex.caltech.edu/Instrument.html)






